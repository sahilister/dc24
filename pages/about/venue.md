---
name: The Conference Venue
---

# Venue

FIXME

## Address

FIXME

# Getting to Busan

## Purchasing your plane ticket

### Compare tickets for two destinations, PUS vs ICN.

* Busan International Airport (PUS) is the nearby airport. But when there's no convenient and reasonable priced ticket to PUS (especially when you are from Europe or Americas), please try to find a ticket to Incheon International Airport (ICN) nearby Seoul. ICN is much bigger and have more flights than PUS, so you will probably find a better ticket.
* When you do a comparision, consider the cost and the time from ICN to Busan. If you use the high speed train (KTX) and the Airport Express Train, the round trip will cost about 120,000 KRW in total. And it took 4 hours to move from ICN to Busan.
* When you are going to arrive at ICN, be careful about arriving and departing time. There is no overnight train in the country; you cannot take train between 11:30 to 05:30. 
	* There is an airport limousine bus couch, but it will take 6 hours and the fare is similar to the high speed train. But for late night arriving or early morning departing, it can be a choice.
	* Another option is, of course, to take time to stay (and take a look around) in a closer location before or after DebConf.

### Ticket with ICN-PUS connection? Again, compare PUS vs ICN.

* You may find a ticket including ICN-PUS connection flight. Before buying it, please compare it and the one arriving ICN without the ICN-PUS connection. Adding ICN-PUS connection is sometimes very expensive like additional 500 USD or needs too much hours to wait at ICN, when taking train to Busan is a better choice.
* The background is,
	* This ICN-PUS flight can be purchased only as a connection of another ICN arriving or departing flight. And the current only operator of the ICN-PUS connection flights is Korean Air. So only Korean Air or SkyTeam codeshare flights can use the ICN-PUS connection.
	* When you are arriving ICN by Korean Air, mostly it doesn't add much cost and you won't wait much. In this case, it's reasonable to buy the ticket with the connection. 
	* But for other SkyTeam codeshare flights, adding this ICN-PUS connection is usually very expensive and/or needs too much time for waiting.

## ICN to Busan

FIXME
